export const getTitle = (account: any): string =>
  `${account.__typename === 'CreditAccount' ? 'Кредитный счёт' : 'Счёт'} ${account.currency}`;
