import { gql } from 'apollo-boost';
export const FRAGMENT_ACCOUNT = gql`
  fragment AccountFragment on AccountItem {
    amount
    currency
    id
    cards {
      list {
        id
        cardNumber
        status
      }
    }
    __typename
  }
`;

export const GET_ACCOUNTS = gql`
  query GetAccounts {
    accounts {
      header {
        total
      }
      list {
        ...AccountFragment
      }
    }
  }
  ${FRAGMENT_ACCOUNT}
`;
