import styled from 'styled-components';

export const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  font-size: 20px;
  padding: 0 20px;
`;

export const Account = styled.div`
  background: #00c0b8;
  color: white;
  padding: 20px;
  border-radius: 30px;
  height: 200px;
  cursor: pointer;
  margin-right: 20px;
  width: 400px;
  position: relative;
  overflow: hidden;
`;

export const Accounts = styled.div`
  display: flex;
  padding: 0 20px;
`;

export const Cards = styled.div`
  display: flex;
  position: absolute;
  bottom: 0;
`;

type CardProps = { isCreated: boolean };

export const Card = styled.div<CardProps>`
  position: relative;
  margin-right: 10px;
  opacity: ${({ isCreated }) => (isCreated ? 1 : 0.5)};

  .cardNumber {
    position: absolute;
    bottom: 28px;
    left: 9px;
    color: white;
  }
`;
