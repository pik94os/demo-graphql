import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { FRAGMENT_ACCOUNT, GET_ACCOUNTS } from './Home.const';
import { GetAccounts, Status } from '../../queries';
import { filter } from 'graphql-anywhere';
import { Account, Accounts, Card, Cards, Title } from './Home.styled';
import { COLORS, H1, H2, H3 } from '@holism/core';
import { InitPlatinumMastercardIcon } from '@holism/icons';
import { useHistory } from 'react-router-dom';
import { Amount } from '../Amount/Amount';
import { getTitle } from '../../helpers';

export const Home = () => {
  const history = useHistory();
  const openAccount = (accountId: string) => (): void => history.push(`/account/${accountId}`);

  const { data } = useQuery<GetAccounts>(GET_ACCOUNTS);

  let accounts = data?.accounts?.list;

  if (accounts) {
    accounts = filter(FRAGMENT_ACCOUNT, accounts);
  }

  const total = data?.accounts?.header?.total;

  return (
    <div>
      <Title>
        <H1>Счета и карты</H1>
        <div>Всего: {total}</div>
      </Title>
      <Accounts>
        {accounts?.map(
          account =>
            account && (
              <Account key={account.id} onClick={openAccount(account.id)}>
                <div>
                  <H2 color={COLORS.white}>
                    {getTitle(account)}
                  </H2>
                </div>
                <H3 color={COLORS.white}>
                  <Amount currency={account.currency} amount={account.amount} color={COLORS.white} />
                </H3>
                <Cards>
                  {account.cards?.list?.slice(-3).map(
                    card =>
                      card && (
                        <Card key={card.id} isCreated={!card?.status || card?.status === Status.CREATED}>
                          <InitPlatinumMastercardIcon size={106} />
                          <div className="cardNumber">{card?.cardNumber?.slice(-4)}</div>
                        </Card>
                      )
                  )}
                </Cards>
              </Account>
            )
        )}
      </Accounts>
    </div>
  );
};
