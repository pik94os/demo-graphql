import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home } from '../Home/Home';
import { Account } from '../Account/Account';
import { Layout } from '../Layout/Layout';
import { client } from '../../core/client';
import { ApolloProvider } from '@apollo/react-hooks';
import { Auth } from '../Auth/Auth';

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Auth>
        <Router>
          <Layout>
            <Switch>
              <Route path="/account/:id">
                <Account />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </Layout>
        </Router>
      </Auth>
    </ApolloProvider>
  );
};

export default App;
