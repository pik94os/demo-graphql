import React from 'react';
import { Button } from '@holism/core';
import { useApolloClient } from '@apollo/react-hooks';

export const Logout = () => {
  const client = useApolloClient();

  const logout = () => {
    client.writeData({ data: { isLoggedIn: false } });
    localStorage.clear();
  };

  return (
    <Button onClick={logout} dimension="medium" color={'primary'}>
      Выход
    </Button>
  );
};
