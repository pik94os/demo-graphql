import { gql } from 'apollo-boost';

export const LOGIN_WITH_PASSWORD = gql`
  mutation LoginWithPassword($login: String!, $password: String!) {
    auth(creds: { login: $login, password: $password }) {
      token
    }
  }
`;
