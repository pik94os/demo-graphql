import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { LOGIN_WITH_PASSWORD } from './Login.const';
import React, { useState } from 'react';
import { LoginWithPassword, LoginWithPasswordVariables } from '../../queries';
import { Button, GridContainer, H1, Input } from '@holism/core';
import { LoginRow } from './Login.styled';

export const Login = () => {
  const client = useApolloClient();
  const [loginWithPassword, { error }] = useMutation<LoginWithPassword, LoginWithPasswordVariables>(
    LOGIN_WITH_PASSWORD,
    {
      onCompleted({ auth }) {
        localStorage.setItem('token', auth?.token || '');
        client.writeData({ data: { isLoggedIn: true } });
      },
      onError(err) {
        console.error(err);
      },
    }
  );
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (e: any) => {
    e.preventDefault();
    return loginWithPassword({ variables: { login, password } });
  };

  const errors: string[] = [];

  error?.graphQLErrors?.forEach(error => {
    if (error.message.indexOf('Invalid username or password') > -1) {
      errors.push('Неверный логин или пароль');
    }
  });

  return (
    <GridContainer>
      <H1>Вход</H1>
      <form onSubmit={handleSubmit}>
        <LoginRow>
          <Input placeholder="Login" value={login} onChange={(e, val) => setLogin(val)} />
        </LoginRow>
        <LoginRow>
          <Input
            placeholder="Password"
            error={errors.length ? errors.join(',') : undefined}
            value={password}
            onChange={(e, val) => setPassword(val)}
            type="password"
          />
        </LoginRow>
        <Button type="submit">Войти</Button>
      </form>
    </GridContainer>
  );
};
