import { Currency } from '../../queries';
import { DollarLineIcon, EuroLineIcon, RoubleLineIcon } from '@holism/icons';
import React, { FC } from 'react';
import { AmountText } from './Amount.styled';

type AmountProps = {
  amount: number | null;
  currency: Currency | null;
  color?: string;
};

export const Amount: FC<AmountProps> = (props: AmountProps) => {
  let CurrencyComponent = RoubleLineIcon;
  if (props.currency === Currency.EUR) {
    CurrencyComponent = EuroLineIcon;
  }
  if (props.currency === Currency.USD) {
    CurrencyComponent = DollarLineIcon;
  }
  return (
    <AmountText>
      {props.amount}
      <CurrencyComponent size={20} color={props.color} />
    </AmountText>
  );
};
