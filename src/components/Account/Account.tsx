import React, { useState } from 'react';
import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { ACCOUNT_INFO_FRAGMENT, CREATE_NEW_CARD, GET_ACCOUNT } from './Account.const';
import { CreateNewCard, CreateNewCardVariables, GetAccount, GetAccountVariables, Status } from '../../queries';
import { Link, useParams } from 'react-router-dom';
import { filter } from 'graphql-anywhere';
import { Button, COLORS, H1, H2, H3 } from '@holism/core';
import { InitPlatinumMastercardIcon, LongArrowLeftIcon, PlusIcon } from '@holism/icons';
import { PayMyPhone } from '../PayMyPhone/PayMyPhone';
import { Amount } from '../Amount/Amount';
import { AccountContainer, AmountBlock, Card, NewCardBlock } from './Account.styled';
import _ from 'lodash';
import { getTitle } from '../../helpers';

export const Account = () => {
  const client = useApolloClient();
  const [isOpenModal, setOpenModal] = useState(false);
  const params: { [key: string]: string } = useParams();
  const id = Number.parseInt(params?.id);

  const { data } = useQuery<GetAccount, GetAccountVariables>(GET_ACCOUNT, {
    skip: Number.isNaN(id),
    variables: { id },
  });

  let account = data?.accounts?.list ? data.accounts.list[0] : null;
  const accountId = Number.parseInt(account?.id || '');

  const [createNewCard] = useMutation<CreateNewCard, CreateNewCardVariables>(CREATE_NEW_CARD, {
    variables: { accountId },
    update(store, { data: newCardData }) {
      const cards = _.get(data, 'accounts.list[0].cards.list') || [];
      if (!cards || !newCardData?.newCard) {
        return;
      }
      const newData: any = data || {};
      _.set(newData, 'accounts.list[0].cards.list', [...cards, newCardData.newCard]);
      client.writeQuery({ query: GET_ACCOUNT, data: newData });
    },
  });

  if (account) {
    account = filter(ACCOUNT_INFO_FRAGMENT, account);
  } else {
    return (
      <div>
        Cчёт не найден. <Link to="/">Назад</Link>
      </div>
    );
  }

  const { limitAmount, overdraft } = account as any;

  return (
    <AccountContainer>
      <AmountBlock>
        <Link to="/">
          <LongArrowLeftIcon size={48} color={COLORS.blue} />
        </Link>
        <H1>{getTitle(account)}</H1>
        <H2>
          <Amount currency={account?.currency || null} amount={account?.amount || null} />
        </H2>
        {!!limitAmount && <H3 color={COLORS.blue}>Кредитный лимит: {limitAmount}</H3>}
        {!!overdraft && <H3 color={COLORS.blue}>Овердрафт: {overdraft}</H3>}
      </AmountBlock>
      <AmountBlock>
        <H1>Карты</H1>
        <div className="cards">
          {account?.cards?.list?.map(card => (
            <Card key={card?.id} isCreated={!card?.status || card?.status === Status.CREATED}>
              <InitPlatinumMastercardIcon className={'card-icon'} />
              <div className="card-number">**** **** **** {card?.cardNumber?.slice(-4)}</div>
              <div className="card-holder">{card?.cardHolder}</div>
            </Card>
          ))}
          <NewCardBlock onClick={() => createNewCard()}>
            <PlusIcon size={106} color={COLORS.white} />
          </NewCardBlock>
        </div>
      </AmountBlock>
      <AmountBlock>
        <H1>Оплатить</H1>
        <Button onClick={() => setOpenModal(true)}>Мой телефон</Button>
      </AmountBlock>
      <PayMyPhone accountId={id} open={isOpenModal} onRequestClose={() => setOpenModal(false)} />
    </AccountContainer>
  );
};
