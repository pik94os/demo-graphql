import styled from 'styled-components';
import { COLORS } from '@holism/core';

export const AccountContainer = styled.div`
  padding: 20px;

  .cards {
    display: flex;
    width: 100%;
    overflow: auto;
  }
`;

type CardProps = { isCreated: boolean };

export const Card = styled.div<CardProps>`
  margin-right: 20px;
  position: relative;
  opacity: ${({ isCreated }) => (isCreated ? 1 : 0.5)};

  .card-number {
    position: absolute;
    top: 30px;
    left: 15px;
    font-size: 20px;
    color: white;
  }

  .card-holder {
    position: absolute;
    bottom: 20px;
    color: white;
    text-transform: uppercase;
    left: 15px;
  }

  .card-icon {
    width: 200px;
  }
`;

export const NewCardBlock = styled.div`
  height: 125px;
  width: 200px;
  border-radius: 7px;
  align-items: center;
  display: flex;
  justify-content: center;
  background: ${COLORS.greyMouse};

  &,
  & > * {
    cursor: pointer;
  }
`;

export const AmountBlock = styled.div`
  margin-bottom: 50px;
`;
