import { gql } from 'apollo-boost';

export const CARD_INFO_FRAGMENT = gql`
  fragment CardInfoFragment on CardItem {
    id
    cardHolder
    cardNumber
    status
  }
`;

export const ACCOUNT_INFO_FRAGMENT = gql`
  fragment AccountInfoFragment on AccountItem {
    id
    amount
    cards {
      list {
        ...CardInfoFragment
      }
    }
    currency
    ... on CreditAccount {
      limitAmount
    }
    ... on CurrentAccount {
      overdraft
    }
    __typename
  }
  ${CARD_INFO_FRAGMENT}
`;

export const GET_ACCOUNT = gql`
  query GetAccount($id: Int) {
    accounts(filter: $id) {
      list {
        ...AccountInfoFragment
      }
    }
  }
  ${ACCOUNT_INFO_FRAGMENT}
`;

export const CREATE_NEW_CARD = gql`
  mutation CreateNewCard($accountId: Int!) {
    newCard(accountId: $accountId) {
      ...CardInfoFragment
    }
  }
  ${CARD_INFO_FRAGMENT}
`;
