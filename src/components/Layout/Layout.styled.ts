import styled from 'styled-components';
import { COLORS } from '@holism/core';

export const Header = styled.div`
  padding: 15px 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.blue};
`;
