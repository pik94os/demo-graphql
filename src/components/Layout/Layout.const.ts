import { gql } from 'apollo-boost';

export const CARD_SUBSCRIPTION = gql`
  subscription CardSubscription {
    card {
      id
      status
    }
  }
`;
