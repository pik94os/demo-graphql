import React from 'react';
import { Header } from './Layout.styled';
import { Logout } from '../Logout/Logout';
import { Hello } from '../Hello/Hello';
import { useSubscription } from '@apollo/react-hooks';
import { CARD_SUBSCRIPTION } from './Layout.const';

export const Layout = ({ children }: any) => {
  useSubscription(CARD_SUBSCRIPTION);
  return (
    <div>
      <Header>
        <Hello />
        <Logout />
      </Header>
      <div>{children}</div>
    </div>
  );
};
