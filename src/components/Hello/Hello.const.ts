import { gql } from 'apollo-boost';

export const USER_FRAGMENT = gql`
  fragment UserFragment on User {
    login
    name
  }
`;

export const GET_USER_INFO = gql`
  query GetUserInfo {
    user {
      ...UserFragment
    }
  }
  ${USER_FRAGMENT}
`;
