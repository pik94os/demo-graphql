import { useQuery } from '@apollo/react-hooks';
import { GET_USER_INFO } from './Hello.const';
import React from 'react';
import { GetUserInfo } from '../../queries';
import { HeaderText } from './Hello.styled';

export const Hello = () => {
  const { data } = useQuery<GetUserInfo>(GET_USER_INFO);
  const name = data?.user?.name || data?.user?.login;
  return <HeaderText>Привет, {name}</HeaderText>;
};
