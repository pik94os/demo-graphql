import styled from 'styled-components';

export const HeaderText = styled.div`
  color: white;
  font-size: 25px;
`;
