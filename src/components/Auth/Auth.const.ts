import { gql } from 'apollo-boost';

export const CHECK_AUTH = gql`
  query CheckAuth {
    isLoggedIn @client
  }
`;
