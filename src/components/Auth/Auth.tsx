import { useQuery } from '@apollo/react-hooks';
import React, { ReactElement } from 'react';
import { CHECK_AUTH } from './Auth.const';
import { CheckAuth } from '../../queries';
import { Login } from '../Login/Login';

type PropTypes = {
  children: ReactElement;
};

export const Auth = ({ children }: PropTypes) => {
  const { data } = useQuery<CheckAuth>(CHECK_AUTH);
  const isLoggedIn = data?.isLoggedIn;
  return isLoggedIn ? children : <Login />;
};
