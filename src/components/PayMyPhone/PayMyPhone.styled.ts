import styled from 'styled-components';

export const ModalContainer = styled.div`
  padding: 20px;
  border-radius: 10px;

  .buttons {
    margin-top: 20px;
  }
`;
