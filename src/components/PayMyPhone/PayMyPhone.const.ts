import { gql } from 'apollo-boost';

export const PAY_PHONE = gql`
  mutation PayPhone($amount: Int!, $accountId: Int!) {
    PayPhone: spendMoney(amount: $amount, accountId: $accountId) {
      id
      amount
    }
  }
`;
