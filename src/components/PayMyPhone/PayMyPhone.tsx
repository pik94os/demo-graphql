import { Button, H1, InputNumber, ModalWindow, Paragraph } from '@holism/core';
import React, { FormEvent, useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { PAY_PHONE } from './PayMyPhone.const';
import { PayPhone, PayPhoneVariables } from '../../queries';
import { ModalContainer } from './PayMyPhone.styled';

type PayMyPhoneProps = { open: boolean; onRequestClose: () => void; accountId: number };

export const PayMyPhone = ({ accountId, open, onRequestClose }: PayMyPhoneProps) => {
  const [amount, setAmount] = useState('0');
  const [payPhone, { loading, error }] = useMutation<PayPhone, PayPhoneVariables>(PAY_PHONE);

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    await payPhone({ variables: { amount: Number.parseInt(amount), accountId } });
    onRequestClose();
  };

  return (
    <ModalWindow
      shouldCloseOnOverlayClick={true}
      shouldCloseOnEsc={true}
      isOpen={open}
      onRequestClose={onRequestClose}
      overlayStyle={{ position: 'absolute' }}
    >
      <ModalContainer>
        <form onSubmit={onSubmit}>
          <H1>Оплата мобильной связи</H1>
          <Paragraph size={18}>Номер телефона: +7 (987) 654-32-10</Paragraph>
          <InputNumber
            placeholder="Введите сумму"
            value={amount}
            onChange={(e, val) => setAmount(val)}
            error={error?.graphQLErrors?.length ? 'Ошибка оплаты' : undefined}
          />
          <div className="buttons">
            <Button type="submit" disabled={!+amount || loading}>
              Оплатить
            </Button>
            <Button color="secondary" onClick={onRequestClose}>
              Отменить
            </Button>
          </div>
        </form>
      </ModalContainer>
    </ModalWindow>
  );
};
