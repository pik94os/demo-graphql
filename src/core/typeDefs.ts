import { gql } from 'apollo-boost';

export const typeDefs = gql`
  extend type Query {
    isLoggedIn: Boolean!
  }
  extend type Mutation {
    getToken(login: String!, password: String!): User
  }
`;
